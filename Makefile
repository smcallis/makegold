################################################################################
# This is a general-purpose Makefile for building C/C++ projects.  
# 
# Some features contained within:
#   * automatic dependency tracking, header files are parsed out of source 
#     and targets generated so that when they change, only the needed binaries
#     are re-compiled.
#   * distribution tarball generation (make dist)
#   * header file verification (make hcheck)
################################################################################

# compiler configuration
CC := g++
override CCOPTS := -std=c++11 -Wall -Wextra -Iinc/ -O3 $(CCOPTS)
override LDOPTS := $(LDOPTS)

# linux requires -lrt, OSX does not
UNAME := $(shell uname -s)
ifeq ($(UNAME),Linux)
    override LDOPTS += -lrt
endif

# define project name and help.  This will tell us the tarball to generate
# and provide help text to print when "make help" is run
PROJECT_NAME := example
PROJECT_HELP := "Help text goes here, will be followed by target documentation"

# compile programs
PROGRAMS := $(patsubst src/%.cc, bin/%, $(wildcard src/*.cc))


################################################################################
# Dependency generation stuff
# reference: http://make.mad-scientist.net/papers/advanced-auto-dependency-generation
################################################################################

# dependency configuration
DEPDIR := .deps
DEPOPT  = -MT $@ -MM -MP -MF $(DEPDIR)/$*.d

# make sure $(DEPDIR) exists
$(shell mkdir -p $(DEPDIR) >/dev/null)


################################################################################
# Define targets
################################################################################
all: $(PROGRAMS)              		 	   ## (default target) build everything

debug:    CCOPTS += -ggdb3 -O0   	           ## build with debug flags on
debug:    all

coverage: CCOPTS += -fprofile-arcs -ftest-coverage ## build with coverage flags (makes debug build)
coverage: debug

profile:  CCOPTS += -pg                            ## build with gprof flags
profile:  all


# General binary target for C++
bin/% : src/%.cc Makefile $(DEPDIR)/%.d
	@rm -f $@
	@$(CC) $(CCOPTS) $(DEPOPT) $< # make dependencies
ifndef VERBOSE
	@echo compiling $@
	@$(CC) $(CCOPTS) $< -o $@ $(LDOPTS)
else
	 $(CC) $(CCOPTS) $< -o $@ $(LDOPTS)
endif

dist: $(PROJECT_NAME).tar.xz                       ## build project distribution tarball


$(PROJECT_NAME).tar.xz: all
	@echo Building $(PROJECT_NAME).tar.xz for distribution
	@rm -f $(PROJECT_NAME).tar.xz
	@rm -rf /tmp/$(PROJECT_NAME)
	@cp -r ../$(PROJECT_NAME) /tmp
	@cd /tmp/; make -C $(PROJECT_NAME) realclean; rm -rf $(PROJECT_NAME)/.git; rm -rf $(PROJECT_NAME)/.objs; tar -cf - $(PROJECT_NAME) | xz -9 -c - > $(PROJECT_NAME).tar.xz
	@mv /tmp/$(PROJECT_NAME).tar.xz ./
	@rm -rf /tmp/$(PROJECT_NAME)


depclean:                      			   ## remove dependency info
	@rm -rf $(DEPDIR)


clean:                         			   ## clean normal build detritus, but not dependency info
	@rm -f $(PROGRAMS)
	@rm -f $(LIBRARIES)


realclean: clean depclean      			   ## clean everything and remove depenency directory


remake: clean all              			   ## clean everything and rebuild


# hcheck attempts to compile each header file as a standalone binary
# this is useful for ensuring consistency, and that each headerfile properly
# includes all its required dependencies so it can easily be sliced out.
HEADERSRC = $(wildcard inc/*.h)
HEADEROBJ = $(patsubst %.h, %.h.o, $(HEADERSRC))

%.h.cc: %.h
	@cat $< | grep -v "#pragma once" > $@

%.h.o: %.h.cc
	@echo compiling $(basename $@ .o)
	@$(CC) $(CCOPTS) -c $< -o $@
	@rm $@ $<

.PHONY: hcheck
hcheck: $(HEADEROBJ)           			   ## run header check to verify consistency and isolation of headers


# print help string 
COLWIDTH = 15

.PHONY: help
help:                          			   ## print this message and exit
	@echo $(PROJECT_HELP)
	@echo
	@echo "Useful command-line variables:"
	@echo "  VERBOSE=1 -- enable verbose mode, print compilation commands"
	@echo
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:[[:space:]]*.*?## / {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST) | sort


# include dependency info
$(DEPDIR)/%.d: ;
.PRECIOUS: $(DEPDIR)/%.d
-include $(wildcard $(patsubst bin/%,$(DEPDIR)/%.d,$(PROGRAMS)))
